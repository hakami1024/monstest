package com.hakami.monstest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MainActivity extends Activity
{
	//private String TAG = "TestActivity";
	
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_main );
		
		Button beginButton = (Button)findViewById(R.id.begin_button);
		beginButton.setOnClickListener( new OnClickListener()
		{

			@Override
			public void onClick( View v )
			{
				startActivity( new Intent( MainActivity.this, TestActivity.class ));
				finish();
			}
			
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu )
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate( R.menu.main, menu );
		return true;
	}	
}
