package com.hakami.monstest;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ResultActivity extends Activity
{

	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_rezult );
		Bundle extras = getIntent().getExtras();
		((TextView)findViewById(R.id.result_textview)).setText( extras.getInt( "com.hakami.monstest.textId" ) );
		((ImageView)findViewById(R.id.result_imageview)).setImageResource( extras.getInt( "com.hakami.monstest.imageId" ));
		Button mButton = (Button)findViewById(R.id.to_begin_button);
		mButton.setOnClickListener( new OnClickListener()
		{

			@Override
			public void onClick( View v )
			{
				startActivity( new Intent( ResultActivity.this, TestActivity.class ));
				finish();
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu )
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate( R.menu.main, menu );
		return true;
	}

}
