package com.hakami.monstest;

import java.text.Format;
import java.util.Arrays;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class TestActivity extends Activity
{

	private RadioGroup mRadioGroup;
	private TextView mQuestionTextView;
	private ImageButton mBackButton;
	private ImageButton mNextButton;
	Question[] QuestionBank = new Question[]
	{
			new Question( R.string.question_01, R.string.answer_01_1, R.string.answer_01_2, R.string.answer_01_3, R.string.answer_01_4, R.string.answer_01_5),
			new Question( R.string.question_02, R.string.answer_02_5, R.string.answer_02_1, R.string.answer_02_2, R.string.answer_02_3, R.string.answer_02_4),
			new Question( R.string.question_03, R.string.answer_03_4, R.string.answer_03_5, R.string.answer_03_1, R.string.answer_03_2, R.string.answer_03_3),
			new Question( R.string.question_04, R.string.answer_04_3, R.string.answer_04_4, R.string.answer_04_5, R.string.answer_04_1, R.string.answer_04_2)			
	};
	private int[] mRadioButtonsId = new int[]
	{
			R.id.button_1,
			R.id.button_2,
			R.id.button_3,
			R.id.button_4,
			R.id.button_5
	};

	private int mCurrentIndex = 0;
	private int[] mButtonPressed = new int[ QuestionBank.length ];
	private String TAG = "TestActivity";
	
	@Override
	protected void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_test );
		
		if( savedInstanceState != null )
		{
			mCurrentIndex = savedInstanceState.getInt( "mCurrentIndex" );
			mButtonPressed = savedInstanceState.getIntArray( "mButtonPressed" );
		}
		else
		{
			Arrays.fill( mButtonPressed, -1 );
		}
		
		OnClickListener goToNextListener = new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				if( mCurrentIndex != QuestionBank.length-1) 
				{
					mCurrentIndex++;
					updateQuestion();
				}
				else
					calculateResult();					
			}
		};
		
		mQuestionTextView = (TextView)findViewById( R.id.testView );
		mQuestionTextView.setOnClickListener( goToNextListener );

		mRadioGroup = (RadioGroup)findViewById( R.id.radioGroup1 );
		mRadioGroup.setOnCheckedChangeListener( new OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged( RadioGroup group, int checkedId )
			{
				int x = Arrays.binarySearch( mRadioButtonsId, checkedId );
				mButtonPressed[ mCurrentIndex ] = ( x>= 0 ) ? x : -1 ;
			}
		});
		
		mBackButton = (ImageButton)findViewById( R.id.back_button );
		mBackButton.setOnClickListener( new OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				if( mCurrentIndex > 0 ) 
					mCurrentIndex-- ;
				updateQuestion();
			}
		});
		
		mNextButton = (ImageButton)findViewById( R.id.next_button );
		mNextButton.setOnClickListener( goToNextListener );

		updateQuestion();
	}

	@Override
	public boolean onCreateOptionsMenu( Menu menu )
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate( R.menu.main, menu );
		return true;
	}
	
	@Override
	public void onSaveInstanceState( Bundle savedInstanceState )
	{
		super.onSaveInstanceState( savedInstanceState );
		savedInstanceState.putInt( "mCurrentIndex", mCurrentIndex );
		savedInstanceState.putIntArray( "mButtonPressed", mButtonPressed );
	}
		
	private void updateQuestion()
	{
		mQuestionTextView.setText( QuestionBank[mCurrentIndex].getQuestion() );
		
		
		for( int i=0; i<mRadioButtonsId.length ; i++ )
		{
			((RadioButton)(mRadioGroup.getChildAt( i ) )).setText( QuestionBank[ mCurrentIndex ].getAnswers()[i] );
			float density = TestActivity.this.getResources().getDisplayMetrics().scaledDensity;
			((RadioButton)(mRadioGroup.getChildAt( i ) )).setTextSize( (mQuestionTextView.getTextSize())/density );
			((RadioButton)(mRadioGroup.getChildAt( i ) )).setTextColor( mQuestionTextView.getTextColors().getDefaultColor() );
			Log.i(TAG, ""+((RadioButton)(mRadioGroup.getChildAt( i ) )).getTextSize()+" / "+mQuestionTextView.getTextSize());
		}
		
		if( mButtonPressed[ mCurrentIndex ] == -1 )
			mRadioGroup.clearCheck();
		else 
			mRadioGroup.check( mRadioButtonsId[ mButtonPressed[ mCurrentIndex ] ] );
	}
	
	private void calculateResult()
	{
		boolean filled = true;
		
		int[] mAnsNums = new int[ mRadioButtonsId.length ];
		int ind;
		for( int i=0; i<QuestionBank.length; i++ )
		{
			if( mButtonPressed[i] == -1 )
			{
				Toast.makeText( TestActivity.this, "Ответы даны не на все вопросы", Toast.LENGTH_LONG ).show();
				filled = false;
				break;
			}
			ind = ( mButtonPressed[i] + 5 - i )%5;
			mAnsNums[ ind ]++;
			if( i == QuestionBank.length-1 && mAnsNums[ ind ] > 0 )
				mAnsNums[ ind ]++;
			Log.d( TAG, " mAnsNums["+i+"] = "+mAnsNums[i] );
		}  

		if( filled )
		{
			int rezVal = 0;
			int rezMark = 0;
			
			for( int i=QuestionBank.length; i>=0; i-- )
			{
				Log.d( TAG, "calculateResult, mAnsNums["+i+"] = "+mAnsNums[i] );
				if( rezVal < mAnsNums[i] )
					{
						rezVal = mAnsNums[i];
						rezMark = i;
					}
			}
					
			Intent resultIntent = new Intent( TestActivity.this, ResultActivity.class );
			int rezTextId, rezImageId;
			
			switch( rezMark )
			{
				case 0:
					rezTextId = R.string.rezult_1;
					rezImageId = R.drawable.monstr1;
					break;
				case 1:
					rezTextId = R.string.rezult_2;
					rezImageId = R.drawable.monstr2;
					break;
				case 2:
					rezTextId = R.string.rezult_3;
					rezImageId = R.drawable.monstr3;
					break;
				case 3:
					rezTextId = R.string.rezult_4;
					rezImageId = R.drawable.monstr4;
					break;
				default:
					rezTextId = R.string.rezult_5;
					rezImageId = R.drawable.monstr5;
			}
			resultIntent.putExtra( "com.hakami.monstest.textId", rezTextId );
			resultIntent.putExtra( "com.hakami.monstest.imageId", rezImageId );
			startActivity( resultIntent );
			finish();
		}
	}

	
}
