package com.hakami.monstest;

public class Question
{

	private int mQuestion;
	private int[] mAnswers = new int[5];
	
	Question( int qId, int ans1, int ans2, int ans3, int ans4, int ans5  )
	{
		mQuestion = qId;
		mAnswers[0] = ans1;
		mAnswers[1] = ans2;
		mAnswers[2] = ans3;
		mAnswers[3] = ans4;
		mAnswers[4] = ans5;
	}
	
	public int getQuestion()
	{
		return mQuestion;
	}
	public void setQuestion( int question )
	{
		mQuestion = question;
	}
	public int[] getAnswers()
	{
		return mAnswers;
	}
	public void setAnswers( int[] answers )
	{
		mAnswers = answers;
	}
	
}
